package main

import (
	"image/color"
	"math/rand"
	"time"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

type apple struct {
	x, y float64
}

func (maca *apple) posicionar() {
	rand.Seed(time.Now().UnixNano())
	maca.x = float64(rand.Intn(64) * 10)
	maca.y = float64(rand.Intn(48) * 10)
}

func (maca apple) verificaColisao(cobra snake) bool {
	colidui := false
	if cobra.cabeça[0] == maca.x && cobra.cabeça[1] == maca.y {
		colidui = true
	}

	for _, elemento := range cobra.corpo {
		if elemento[0] == maca.x && elemento[1] == maca.y {
			colidui = true
		}
	}
	return colidui
}

func (maca apple) desenhar(janela *ebiten.Image) {
	ebitenutil.DrawRect(janela, maca.x, maca.y, 10, 10, color.RGBA{255, 0, 0, 255})
}
