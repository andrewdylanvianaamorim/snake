package main

import (
	"fmt"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

var cobra = snake{0, 0, [2]float64{100, 100}, [][2]float64{[2]float64{90, 100}, [2]float64{90, 100}}}
var maca = apple{200, 200}

func atualiza(janela *ebiten.Image) error {
	if cobra.verificaColisao() == false {
		ebiten.SetMaxTPS(20)
		maca.desenhar(janela)
		cobra.desenhar(janela)
		cobra.mover()
		if maca.verificaColisao(cobra) {
			maca.posicionar()
			cobra.crescer()
			cobra.almentarPontos()
		}
	} else {
		ebitenutil.DebugPrintAt(janela, fmt.Sprintf("Você Morreu! Sua pontuação foi de %d\nAperte a tecla r para reiniciar o jogo.", cobra.pontos), 180, 200)
		if ebiten.IsKeyPressed(ebiten.KeyR) {
			cobra = snake{0, 0, [2]float64{100, 100}, [][2]float64{[2]float64{90, 100}, [2]float64{90, 100}}}
			maca = apple{200, 200}
		}
	}
	return nil
}

func main() {
	if err := ebiten.Run(atualiza, 640, 480, 1, "Snake Game"); err != nil {
		panic(err)
	}
}
