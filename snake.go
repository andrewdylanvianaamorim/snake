package main

import (
	"image/color"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

type snake struct {
	pontos, direcao int
	cabeça          [2]float64
	corpo           [][2]float64
}

func (cobra *snake) mover() {

	switch {
	case ebiten.IsKeyPressed(ebiten.KeyW):
		if cobra.direcao != 2 {
			cobra.direcao = 1
		}
	case ebiten.IsKeyPressed(ebiten.KeyS):
		if cobra.direcao != 1 {
			cobra.direcao = 2
		}
	case ebiten.IsKeyPressed(ebiten.KeyA):
		if cobra.direcao != 4 {
			cobra.direcao = 3
		}
	case ebiten.IsKeyPressed(ebiten.KeyD):
		if cobra.direcao != 3 {
			cobra.direcao = 4
		}
	}

	switch cobra.direcao {
	case 0:

	case 1:
		cobra.cabeça[1] -= 10
	case 2:
		cobra.cabeça[1] += 10
	case 3:
		cobra.cabeça[0] -= 10
	case 4:
		cobra.cabeça[0] += 10
	}

	var valorAnterior, valorAnterior2 [2]float64
	valorAnterior = cobra.cabeça
	for contador := len(cobra.corpo) - 1; contador >= 0; contador-- {
		valorAnterior2 = cobra.corpo[contador]
		cobra.corpo[contador] = valorAnterior
		valorAnterior = valorAnterior2
	}
}

func (cobra snake) desenhar(janela *ebiten.Image) {
	ebitenutil.DrawRect(janela, cobra.cabeça[0], cobra.cabeça[1], 10, 10, color.RGBA{0, 0, 255, 255})
	for contador := len(cobra.corpo) - 1; contador >= 0; contador-- {
		ebitenutil.DrawRect(janela, cobra.corpo[contador][0], cobra.corpo[contador][1], 10, 10, color.RGBA{0, 0, 255, 255})
	}
}

func (cobra *snake) crescer() {
	cobra.corpo = append(cobra.corpo, [2]float64{
		cobra.corpo[len(cobra.corpo)-1][0],
		cobra.corpo[len(cobra.corpo)-1][1],
	})
}

func (cobra *snake) almentarPontos() {
	cobra.pontos += 10
}

func (cobra *snake) verificaColisao() bool {
	colidiu := false
	if cobra.cabeça[0] > 630 || cobra.cabeça[0] < 0 {
		colidiu = true
	} else if cobra.cabeça[1] > 470 || cobra.cabeça[1] < 0 {
		colidiu = true
	}
	
	if len(cobra.corpo) > 10{
		for c := 0;c<len(cobra.corpo) - 3;c++{
			if cobra.corpo[c] == cobra.cabeça{
				colidiu = true
			}
		}
	}
	return colidiu
}
